package com.haidev.tvmoviecatalog.main.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.haidev.tvmoviecatalog.menu.movie.views.MovieFragment
import com.haidev.tvmoviecatalog.menu.tv.views.TvFragment

class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val pages = listOf(
        MovieFragment(),
        TvFragment()
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }


}