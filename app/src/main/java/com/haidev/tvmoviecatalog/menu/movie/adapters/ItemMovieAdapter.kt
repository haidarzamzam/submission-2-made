package com.haidev.tvmoviecatalog.menu.movie.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.haidev.tvmoviecatalog.R
import com.haidev.tvmoviecatalog.menu.movie.models.MovieModel
import com.haidev.tvmoviecatalog.menu.movie.views.DetailMovieActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list_movie.view.*

class ItemMovieAdapter(val context: Context, private val movies: List<MovieModel>) :
    RecyclerView.Adapter<MovieHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MovieHolder {
        return MovieHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_list_movie, viewGroup, false))
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        holder.bindMovie(movies[position], context)
    }

}

class MovieHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val tvMovieName = view.txtNamaMovie
    private val tvMovieDate = view.txtTglMovie
    private val tvMovieDesc = view.txtDeskripsi
    private val imgMovie = view.ivImageMovie
    private val cvCard = view.itemCard

    fun bindMovie(movie: MovieModel, context: Context) {
        tvMovieName.text = movie.title
        tvMovieDate.text = movie.dateRelease
        tvMovieDesc.text = movie.desc
        Picasso.get().load(movie.image).into(imgMovie)

        cvCard.setOnClickListener {
            val intent = Intent(context, DetailMovieActivity::class.java)
            intent.putExtra("extra_movie", movie)
            context.startActivity(intent)
        }
    }
}