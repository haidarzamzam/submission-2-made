package com.haidev.tvmoviecatalog.menu.movie.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieModel(
    var image: String,
    var title: String,
    var dateRelease: String,
    var desc: String,
    var writer: String,
    var director: String
) : Parcelable
