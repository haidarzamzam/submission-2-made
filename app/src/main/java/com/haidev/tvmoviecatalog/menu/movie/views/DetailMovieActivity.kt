package com.haidev.tvmoviecatalog.menu.movie.views

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.haidev.tvmoviecatalog.R
import com.haidev.tvmoviecatalog.menu.movie.models.MovieModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_movie.*

class DetailMovieActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val item = intent.getParcelableExtra<MovieModel>("extra_movie")
        supportActionBar?.title = getString(R.string.txt_detail) + " " + item.title

        Picasso.get().load(item.image).into(ivMovie)
        txtTitle.text = item.title
        txtDate.text = item.dateRelease
        txtDesc.text = item.desc
        txtWriter.text = item.writer
        txtDirector.text = item.director
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
