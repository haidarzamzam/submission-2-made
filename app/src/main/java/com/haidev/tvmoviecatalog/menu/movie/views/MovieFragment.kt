package com.haidev.tvmoviecatalog.menu.movie.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.tvmoviecatalog.R
import com.haidev.tvmoviecatalog.menu.movie.adapters.ItemMovieAdapter
import com.haidev.tvmoviecatalog.menu.movie.models.MovieModel
import kotlinx.android.synthetic.main.fragment_movie.*

class MovieFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val listMovie = listOf(
            MovieModel(
                image = "https://image.tmdb.org/t/p/w1280/bdrngRQ8m5wYPGApSBQZJL5Ulmc.jpg",
                title = "Soekarno",
                dateRelease = "December 11, 2013",
                desc = "942, The Nederlands(ch)-Indië Government in Java Island captured Soekarno, an aspiring young man who wants to free Indonesia from colonialism. He was then put in Banceuy Prison at Bandung, Indonesia. Instead of lamenting, Soekarno found a way to fight back by delivering his famous defence oration \"Indonesi Acccuse!\" in his trial at Bandung Laandraad Courthouse. This story follows the life of Soekarno, Republic of Indonesia's first president, from his childhood until he managed to proclaimed Indonesian freedom with M. Hatta.",
                writer = "Hanung Bramantyo",
                director = "Ben Sihombing"
            ),
            MovieModel(
                image = "https://image.tmdb.org/t/p/w1280/s668M60qCrNOUDEEJLHpcWRa0Gz.jpg",
                title = "Red & White",
                dateRelease = "August 13, 2009",
                desc = "The movie starts with the tough lives of the aforementioned characters in a military school, showing friendship and even competition among them. But the lives of the young cadets changes when Dutch soldiers suddenly attack their camp, tearing them apart. With limited weaponry and forces, they decided to unite to fight against the Dutch and defend their freedom. Combining action, drama, humor, romance, human tragedy and strong personal stories, the movie is aimed at inspiring the new generation with the spirit of the generations before them who fought and sacrificed themselves for the freedoms Indonesia enjoys today.",
                writer = "Yadi Sugandi",
                director = "Conor Allyn"
            ),
            MovieModel(
                image = "https://image.tmdb.org/t/p/original/mfDGRp19BcZUfPIhdqzdQ2uGZrf.jpg",
                title = "The Raid",
                dateRelease = "September 8, 2011",
                desc = "Deep in the heart of Jakarta's slums lies an impenetrable safe house for the world's most dangerous killers and gangsters. Until now, the run-down apartment block has been considered untouchable to even the bravest of police. Cloaked under the cover of pre-dawn darkness and silence, an elite swat team is tasked with raiding the safe house in order to take down the notorious drug lord that runs it. But when a chance encounter with a spotter blows their cover and news of their assault reaches the drug lord, the building's lights are cut and all the exits blocked. Stranded on the sixth floor with no way out, the unit must fight their way through the city's worst to survive their mission. Starring Indonesian martial arts sensation Iko Uwais.",
                writer = "Gareth Evans",
                director = "Gareth Evans"
            ),
            MovieModel(
                image = "https://image.tmdb.org/t/p/original/5CY2Aiv0m26P0Si5oZ8RVcS8z02.jpg",
                title = "Jendral Soedirman",
                dateRelease = "August 27, 2015",
                desc = "The Dutch declare unilaterally that they are not longer bound by the Renville Treaty, and to stop the ceasefire. On December 19, 1948, Army Commander General Spoor Noord Simons leads military aggression II to attack Yogyakarta, the capital of the Republic of Indonesia at the time. The Dutch arrested Soekarno-Hatta and exiled them to the island of Bangka. General Soedirman who suffered from tuberculosis leads the guerrilla war for seven months against the Dutch.",
                writer = "Deddy Safiudin",
                director = "Viva Westi"
            ),
            MovieModel(
                image = "https://image.tmdb.org/t/p/original/wqgKvJDNELWmLk9nwiKiW3k84MH.jpg",
                title = "The Rainbow Troops",
                dateRelease = "September 25, 2008",
                desc = "In the 1970s, a group of 10 students struggles with poverty and develop hopes for the future in Gantong Village on the farming and tin mining island of Belitung off the east coast of Sumatra.",
                writer = "Riri Riza",
                director = "Riri Riza"
            ),
            MovieModel(
                image = "https://image.tmdb.org/t/p/original/v9bU65T8uFRGgeugu8lmHto5adv.jpg",
                title = "What's Up with Love?",
                dateRelease = "February 8, 2002",
                desc = "Cinta, a teenager in suburban Jakarta, spends all of her time with her four girlfriends, Maura, Alya, Carmen and Milly - that is, until she falls for Rangga, the unassuming winner of the school poetry contest. Rangga's presence triggers the jealousy of Cinta's best friends, and things get more challenging for the couple when the girls pressure Cinta to choose between them and Rangga. A sharply etched yet humorous love story of growing infatuation, ambivalence and near misunderstanding between two idealistic teenagers, Cinta and Rangga, at a high school in Jakarta. This sophisticated and charming film provides an engaging portrait of middle-class teenagers.",
                writer = "Mira Lesmana",
                director = "Rudy Soedjarwo"
            ),
            MovieModel(
                image = "https://image.tmdb.org/t/p/original/eitRZXfbw6rO0CfP3lPaGgK63qr.jpg",
                title = "Dilan 1990",
                dateRelease = "January 25, 2018",
                desc = "It was 1990. Milea just moved from Jakarta to Bandung. She met a boy named Dilan. Soon they will face a great journey upon them.",
                writer = "Pidi Baiq",
                director = "Fajar Bustomi"
            ),
            MovieModel(
                image = "https://image.tmdb.org/t/p/original/vYJTJzwS81gXTDV28U2zxrFtEi5.jpg",
                title = "Gie",
                dateRelease = "July 13, 2005",
                desc = "Gie is a 2005 Indonesian film directed by Riri Riza. The film tells the story of Soe Hok Gie, a graduate from University of Indonesia who is known as an activist and nature lover. The film is based on a diary Catatan Seorang Demonstran written by Soe himself. The plot of this film is an interpretation of the filmmakers, and scenes portraying Soe's private life may be partly fictionalised for dramatisation.",
                writer = "Riri Riza",
                director = "Riri Riza"
            ),
            MovieModel(
                image = "https://image.tmdb.org/t/p/original/eOdYhBFF7vE5v83KVVQfDEyLgEu.jpg",
                title = "Habibie & Ainun",
                dateRelease = "December 19, 2012",
                desc = "A biopic of Indonesia’s third president, Bacharuddin Jusuf Habibie, illuminates his lifelong devotion to his wife. The movie, Habibie & Ainun, opens with a rickshaw ride on a rainy night in Bandung, West Java, in the 1960s. Habibie (played by Reza Rahadian) asks Ainun (Bunga Citra Lestari) to marry him and to join him on his trip to Germany.",
                writer = "Ifan Adriansyah Ismail",
                director = "Faozan Rizal"
            ),
            MovieModel(
                image = "https://image.tmdb.org/t/p/original/g6OG2MkBNGfEM7r5D9JGvHTWvDA.jpg",
                title = "The Dreamer",
                dateRelease = "December 17, 2009",
                desc = "This movie is an adaptation from a popular novel by the same title (Sang Pemimpi a.k.a The Dreamer). It portrays two village boys with a dream of traveling to Paris, to study at Sorbonne University.",
                writer = "Mira Lesmana",
                director = "Riri Riza"
            )
        )

        val movieAdapter = context?.let { ItemMovieAdapter(it, listMovie) }

        rvListMovie.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = movieAdapter
        }
    }
}
