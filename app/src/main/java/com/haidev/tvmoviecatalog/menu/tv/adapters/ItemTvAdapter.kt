package com.haidev.tvmoviecatalog.menu.tv.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.haidev.tvmoviecatalog.R
import com.haidev.tvmoviecatalog.menu.tv.models.TvModel
import com.haidev.tvmoviecatalog.menu.tv.views.DetailTvActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list_tv.view.*

class ItemTvAdapter(val context: Context, private val tv: List<TvModel>) : RecyclerView.Adapter<TvHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): TvHolder {
        return TvHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_list_tv, viewGroup, false))
    }

    override fun getItemCount(): Int = tv.size

    override fun onBindViewHolder(holder: TvHolder, position: Int) {
        holder.bindTv(tv[position], context)
    }

}

class TvHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val tvTvName = view.txtNamaTv
    private val tvTvDate = view.txtTglTv
    private val tvTvDesc = view.txtDeskripsiTv
    private val imgTv = view.ivImageTv
    private val cvTvCard = view.itemTvCard

    fun bindTv(tv: TvModel, context: Context) {
        tvTvName.text = tv.title
        tvTvDate.text = tv.year
        tvTvDesc.text = tv.desc
        Picasso.get().load(tv.image).into(imgTv)

        cvTvCard.setOnClickListener {
            val intent = Intent(context, DetailTvActivity::class.java)
            intent.putExtra("extra_tv", tv)
            context.startActivity(intent)
        }
    }
}