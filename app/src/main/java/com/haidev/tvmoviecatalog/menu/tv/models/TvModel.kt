package com.haidev.tvmoviecatalog.menu.tv.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TvModel(
    var image: String,
    var title: String,
    var year: String,
    var desc: String
) : Parcelable