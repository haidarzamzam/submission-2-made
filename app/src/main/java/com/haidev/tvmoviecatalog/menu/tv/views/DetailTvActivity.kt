package com.haidev.tvmoviecatalog.menu.tv.views

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.haidev.tvmoviecatalog.R
import com.haidev.tvmoviecatalog.menu.tv.models.TvModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_tv.*

class DetailTvActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_tv)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val item = intent.getParcelableExtra<TvModel>("extra_tv")
        supportActionBar?.title = getString(R.string.txt_detail) + " " + item.title
        txtTitleTv.text = item.title + " (${item.year})"
        txtDescTv.text = item.desc
        Picasso.get().load(item.image).into(ivTv)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
