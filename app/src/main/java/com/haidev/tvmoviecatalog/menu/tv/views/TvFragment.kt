package com.haidev.tvmoviecatalog.menu.tv.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.tvmoviecatalog.R
import com.haidev.tvmoviecatalog.menu.tv.adapters.ItemTvAdapter
import com.haidev.tvmoviecatalog.menu.tv.models.TvModel
import kotlinx.android.synthetic.main.fragment_tv.*

class TvFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tv, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val tvMovie = listOf(
            TvModel(
                image = "https://image.tmdb.org/t/p/original/cRlYnn2DwSy0ySTwfEYrGtVduZZ.jpg",
                title = "National Geographic Explorer",
                year = "1985",
                desc = "National Geographic Explorer is an American documentary television series that originally premiered on Nickelodeon on April 7, 1985, after having been produced as a less costly and intensive alternative to PBS's National Geographic Specials by Pittsburgh station WQED. The first episode was produced by WQED and featured long-time Explorer camerman Mark Knobil, who is the few staff members with the franchise during all 24 seasons. The program is the longest-running documentary television series on cable television. Presented every Sunday from 5:00 PM to 8:00 PM, the original series was three hours in length, containing five to ten short films. Although the National Geographic Society had been producing specials for television for 20 years prior to Explorer, the premiere of the series required an increase in production from 4 hours of programming a year to 156 hours. Tim Cowling and Tim Kelly were the executive producers for the series during this transition In its 24 years on television, Explorer has worked for five television outlets. In February 1986, Explorer moved to TBS, where it had a successful run until September 1999, when it moved to CNBC. In October 2001, the series moved to MSNBC. In June 2003, the series re-launched itself on MSNBC as Ultimate Explorer, with Lisa Ling as the host. On July 8, 2004, Explorer joined the National Geographic Channel, where currently it airs every Tuesday night at 10:00 PM."
            ),
            TvModel(
                image = "https://image.tmdb.org/t/p/original/ckCQAPQ8gKgFXS8x8IwJQB06cfA.jpg",
                title = "MasterChef",
                year = "2010",
                desc = "This hit cooking competition series sees award-winning chef Gordon Ramsay and other celebrity chefs put a group of contestants through a series of challenges and elimination rounds, in order to turn one home cook into a culinary master."
            ),
            TvModel(
                image = "https://image.tmdb.org/t/p/original/sad5r8q8oyqASXfEb4sk3wXRhkc.jpg",
                title = "MasterChef Junior",
                year = "2013",
                desc = "American version of the culinary competition series that gives talented kids between the ages of 8 and 13 the chance to showcase their culinary abilities and passion for food through a series of delicious challenges. Celebrated food experts coach and encourage the promising hopefuls to cook like pros and teach them the tricks of the trade along the way."
            ),
            TvModel(
                image = "https://image.tmdb.org/t/p/original/grbtxG0peBrACueHOUtOg5Zmta9.jpg",
                title = "The X Factor",
                year = "2004",
                desc = "The X Factor is a British television music competition to find new singing talent, contested by aspiring singers drawn from public auditions. Created by Simon Cowell, the show began in September 2004 and has since aired annually from August/September through to December. The show is produced by FremantleMedia's Thames and Cowell's production company SYCOtv. It is broadcast on the ITV network in the United Kingdom and TV3 in Ireland, with spin-off behind-the-scenes show The Xtra Factor screened on ITV2. It is the originator of the international The X Factor franchise. The X Factor was devised as a replacement for the highly successful Pop Idol, which was put on indefinite hiatus after its second series, largely because Cowell, who was a judge on Pop Idol, wished to launch a show to which he owned the television rights. The perceived similarity between the two shows later became the subject of a legal dispute. The \"X Factor\" of the title refers to the undefinable \"something\" that makes for star quality. The original judging panel was Cowell, Sharon Osbourne and Louis Walsh. Dannii Minogue joined the panel in series 4, and Cheryl Cole replaced Osbourne in series 5 after her departure. After series 7, Cowell and Cole both left to judge the American version of the show whilst Minogue left the show due to commitments on Australia's Got Talent. Kelly Rowland, Tulisa Contostavlos and Gary Barlow then joined Walsh on the judging panel for series 8, though Rowland announced she would not return for series 9 and was replaced by Nicole Scherzinger. Osbourne later returned in 2013, replacing Contostavlos. Since 2007, the show has been presented by Dermot O'Leary, who replaced original host Kate Thornton. The show is split into a series of phases, following the contestants from auditions through to the grand finale. In the original televised audition phase of the show, contestants sang in an audition room in front of just the judges. From series 6 onwards, auditionees sing on a stage in front of the judges and a live audience. Successful auditionees go through to \"bootcamp\" and then to \"judges' houses\", where judges narrow down the acts in their category down to three or four acts to mentor for the live shows, where the public vote for their favourite acts following weekly live performances by the contestants."
            ),
            TvModel(
                image = "https://image.tmdb.org/t/p/original/6wnyxE0lsCbwLT018ImwMSYSMSz.jpg",
                title = "When We Left Earth: The NASA Missions",
                year = "2008",
                desc = "When We Left Earth: The NASA Missions or NASA's Greatest Missions: When We Left Earth in the UK is a Discovery Channel HD documentary miniseries consisting of six episodes documenting American human spaceflight, spanning from the first Mercury flights through the Gemini program to the Apollo moon landings, the Space Shuttle, and the construction of the International Space Station. It was created in association with NASA to commemorate the agency's fiftieth anniversary in 2008. It first aired on June 8, 2008, and concluded on June 22. Each airing consisted of two hour-long episodes. The miniseries was released on DVD on July 10, 2008, and was released on Blu-ray disc on August 12. The third episode, \"Landing the Eagle\", was re-aired on July 20, 2009 for the 40th anniversary of the Apollo 11 moon landing. It featured improved images from the moonwalk."
            ),
            TvModel(
                image = "https://image.tmdb.org/t/p/original/2bZ5c9sHbzApWVIsUoWztg43EgB.jpg",
                title = "WWE Raw",
                year = "1993",
                desc = "WWE Raw is a professional wrestling television program that currently airs live on Monday evenings on the USA Network in the United States. The show debuted on January 11, 1993. WWE Raw moved from the USA Network to TNN in September, 2000 and then to Spike TV in August, 2003 when TNN was rebranded. On October 3, 2005 WWE Raw returned to the USA Network. Since its first episode, WWE Raw has broadcast live from 203 different arenas in 169 cities and towns in ten different nations. As of the show's 1,000th episode, airing on July 23, 2012, WWE Raw has become a three-hour broadcast from two-hours, a format that had previously been reserved for special episodes."
            ),
            TvModel(
                image = "https://image.tmdb.org/t/p/original/iE3s0lG5QVdEHOEZnoAxjmMtvne.jpg",
                title = "One-Punch Man",
                year = "2015",
                desc = "Saitama is a hero who only became a hero for fun. After three years of “special” training, though, he’s become so strong that he’s practically invincible. In fact, he’s too strong—even his mightiest opponents are taken out with a single punch, and it turns out that being devastatingly powerful is actually kind of a bore. With his passion for being a hero lost along with his hair, yet still faced with new enemies every day, how much longer can he keep it going?"
            ),
            TvModel(
                image = "https://image.tmdb.org/t/p/original/4XddcRDtnNjYmLRMYpbrhFxsbuq.jpg",
                title = "Gotham",
                year = "2014",
                desc = "Before there was Batman, there was GOTHAM. Everyone knows the name Commissioner Gordon. He is one of the crime world's greatest foes, a man whose reputation is synonymous with law and order. But what is known of Gordon's story and his rise from rookie detective to Police Commissioner? What did it take to navigate the multiple layers of corruption that secretly ruled Gotham City, the spawning ground of the world's most iconic villains? And what circumstances created them – the larger-than-life personas who would become Catwoman, The Penguin, The Riddler, Two-Face and The Joker?"
            ),
            TvModel(
                image = "https://image.tmdb.org/t/p/original/yTZQkSsxUFJZJe67IenRM0AEklc.jpg",
                title = "The Simpsons",
                year = "1989",
                desc = "Set in Springfield, the average American town, the show focuses on the antics and everyday adventures of the Simpson family; Homer, Marge, Bart, Lisa and Maggie, as well as a virtual cast of thousands. Since the beginning, the series has been a pop culture icon, attracting hundreds of celebrities to guest star. The show has also made name for itself in its fearless satirical take on politics, media and American life in general."
            ),
            TvModel(
                image = "https://image.tmdb.org/t/p/original/lTVhwFcSHqN0Xv8HLxDHILtrwfX.jpg",
                title = "Naruto",
                year = "2002",
                desc = "A powerful beast known as the Nine-Tails attacks Konoha, the hidden leaf village in the Land of Fire, one of the Five Great Shinobi Nations in the Ninja World. In response, the leader of Konoha at the time, the Fourth Hokage, seals the fox inside the body of the new born Naruto Uzumaki, making Naruto a host of the beast; During this process it cost the fourth Hokage his life, requiring the Third Hokage to return from retirement in order to become leader of Konoha again. As a child, Naruto is shunned by the Konoha community, who treat Naruto as if he were the Nine-Tails that attacked the village."
            )
        )

        val tvAdapter = context?.let { ItemTvAdapter(it, tvMovie) }

        rvListTv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = tvAdapter
        }
    }
}
